package main

import (
	"flag"
	"netspace/ClientEndPoint"
	"netspace/Log"
)

func init() {
	Log.NewLogger().Initialize()
}

func main() {
	host := flag.String("host", "124.221.112.180", "server host")
	port := flag.String("port", "9090", "listen port")
	pass := flag.String("pass", "Lsl123456@163.com", "auth password")
	bind := flag.String("bind", "127.0.0.1:80", "your local server host and port")
	flag.Parse()
	c := ClientEndPoint.NewClient(*host, *port, *pass, *bind)
	c.Run()
}
