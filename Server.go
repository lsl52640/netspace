package main

import (
	"flag"
	"netspace/Log"
	"netspace/ServerEndpoint"
)

func init() {
	Log.NewLogger().Initialize()
}

func main() {
	port := flag.String("port", ":9090", "listen port")
	pass := flag.String("pass", "123456", "auth password")
	flag.Parse()
	s := ServerEndpoint.NewServer(*port, *pass)
	s.Run()
}
