BUILD_ENV := CGO_ENABLED=0
BUILD=`date +%FT%T%z`

TARGET_EXEC := netspace

.PHONY: all clean setup build-linux build-osx build-windows

all: clean setup build-linux build-osx build-windows
clean:
	rm -rf build
setup:
	mkdir -p ./build
build-linux:
	${BUILD_ENV} GOARCH=amd64 GOOS=linux go build -o build/${TARGET_EXEC}-server-linux Server.go
	${BUILD_ENV} GOARCH=amd64 GOOS=linux go build -o build/${TARGET_EXEC}-linux Client.go
	cp deploy.sh ./build/deploy.sh
build-osx:
	${BUILD_ENV} GOARCH=amd64 GOOS=darwin go build -o build/${TARGET_EXEC}-server-darwin Server.go
	${BUILD_ENV} GOARCH=amd64 GOOS=darwin go build -o build/${TARGET_EXEC}-mac Client.go
build-windows:
	${BUILD_ENV} GOARCH=amd64 GOOS=windows go build -o build/${TARGET_EXEC}-win.exe Client.go